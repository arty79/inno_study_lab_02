CREATE TABLE crm_user
(
  id serial NOT NULL,
  login character varying(255) NOT NULL,
  user_password character varying(1000) NOT NULL,
  first_name character varying(255) NOT NULL,
  last_name character varying(255) NOT NULL,
  email character varying(255) NOT NULL,
  is_active boolean NOT NULL,
  is_deleted boolean NOT NULL,
  CONSTRAINT pk_user PRIMARY KEY (id)
);

CREATE TABLE role
(
  id serial NOT NULL,
  name character varying(255) NOT NULL,
  code character varying(255) NOT NULL,
  is_deleted boolean NOT NULL,
  CONSTRAINT pk_role PRIMARY KEY (id)
);

CREATE TABLE user_role
(
  id serial NOT NULL,
  crm_user_id integer NOT NULL,
  role_id integer NOT NULL,
  CONSTRAINT pk_user_role PRIMARY KEY (id),
  CONSTRAINT fk_crm_user_role FOREIGN KEY (crm_user_id)
      REFERENCES crm_user (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_user_role_role FOREIGN KEY (role_id)
      REFERENCES role (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);


CREATE TABLE operation
(
  id serial NOT NULL,
  code character varying(255),
  operation_name character varying(255),
  is_deleted boolean NOT NULL,
  CONSTRAINT pk_operation PRIMARY KEY (id)
);

CREATE TABLE resource
(
  id serial NOT NULL,
  code character varying(255),
  name character varying(255),
  url character varying(255),
  is_deleted boolean NOT NULL,
  CONSTRAINT pk_resource PRIMARY KEY (id)
);

CREATE TABLE operation_resource
(
  id serial NOT NULL,
  resource_id integer NOT NULL,
  operation_id integer NOT NULL,
  CONSTRAINT pk_operation_resource PRIMARY KEY (id),
  CONSTRAINT fk_operation_resource_resource FOREIGN KEY (resource_id)
      REFERENCES resource (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_operation_resource_operation FOREIGN KEY (operation_id)
      REFERENCES operation (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE role_resource
(
  id serial NOT NULL,
  resource_id integer NOT NULL,
  role_id integer NOT NULL,
  CONSTRAINT pk_role_resource PRIMARY KEY (id),
  CONSTRAINT fk_role_resource_resource FOREIGN KEY (resource_id)
      REFERENCES resource (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_role_resource_role FOREIGN KEY (role_id)
      REFERENCES role (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);
