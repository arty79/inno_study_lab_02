INSERT INTO crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('shildt','123456', 'John', 'Schildt', '123lalala@java.com', true, false);
INSERT INTO crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('admin','admin', 'Admin', 'Adminovich', 'admin@java.com', true, false);
INSERT INTO crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('user','user', 'User', 'Userovich', 'user@java.com', true, false);
INSERT INTO crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('ashan_kazan','121212', 'Ashan', 'Kazansky', 'ashan_user@java.com', true, false);
INSERT INTO crm_user(login, user_password, first_name, last_name, email, is_active, is_deleted) VALUES ('ashan_msk','7777777', 'Ashan', 'Moskovsky', 'ashan_user@java.com', true, false);

INSERT INTO role(name, code, is_deleted) VALUES ('Admin', 'A', false);
INSERT INTO role(name, code, is_deleted) VALUES ('User', 'U', false);
INSERT INTO role(name, code, is_deleted) VALUES ('Student', 'S', false);
INSERT INTO role(name, code, is_deleted) VALUES ('Observer', 'O', false);

INSERT INTO resource(name, code, is_deleted) VALUES ('Catalog', 'C', false);
INSERT INTO resource(name, code, is_deleted) VALUES ('Journal', 'J', false);
INSERT INTO resource(name, code, is_deleted) VALUES ('Rate', 'R', false);

INSERT INTO operation(operation_name, code, is_deleted) VALUES ('Add', 'A', false);
INSERT INTO operation(operation_name, code, is_deleted) VALUES ('Update', 'U', false);
INSERT INTO operation(operation_name, code, is_deleted) VALUES ('Edit', 'E', false);
INSERT INTO operation(operation_name, code, is_deleted) VALUES ('Delete', 'D', false);

INSERT INTO user_role(crm_user_id, role_id) VALUES (1,1);
INSERT INTO user_role(crm_user_id, role_id) VALUES (3,2);
INSERT INTO user_role(crm_user_id, role_id) VALUES (1,3);

INSERT INTO operation_resource(resource_id, operation_id) VALUES (1,1);
INSERT INTO operation_resource(resource_id, operation_id) VALUES (1,2);
INSERT INTO operation_resource(resource_id, operation_id) VALUES (1,3);
INSERT INTO operation_resource(resource_id, operation_id) VALUES (1,4);
INSERT INTO operation_resource(resource_id, operation_id) VALUES (2,1);
INSERT INTO operation_resource(resource_id, operation_id) VALUES (2,2);
INSERT INTO operation_resource(resource_id, operation_id) VALUES (2,3);
INSERT INTO operation_resource(resource_id, operation_id) VALUES (3,1);
INSERT INTO operation_resource(resource_id, operation_id) VALUES (3,2);
INSERT INTO operation_resource(resource_id, operation_id) VALUES (3,3);


INSERT INTO role_resource(role_id,resource_id) VALUES (1,1);
INSERT INTO role_resource(role_id,resource_id) VALUES (1,2);
INSERT INTO role_resource(role_id,resource_id) VALUES (1,3);
INSERT INTO role_resource(role_id,resource_id) VALUES (2,2);
INSERT INTO role_resource(role_id,resource_id) VALUES (2,3);
INSERT INTO role_resource(role_id,resource_id) VALUES (3,3);
