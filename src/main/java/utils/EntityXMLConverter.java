package utils;

import com.innopolis.examples.xjc.Role;
import com.innopolis.examples.xjc.User;
import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Convert object in XML file
 * and also convert from List of XML file to list of objects
 *
 * @author  Artem Panasyuk
 */
public class EntityXMLConverter {
    private static int count = 0;
    private static final Logger LOG = Logger.getLogger(EntityXMLConverter.class);

    /**
     * Convert from List of XML file from directory to list of objects
     * @param directoryPath
     * @param <T>
     * @return list objects
     */
    public static <T> List<T> convertXMLToEntity(String directoryPath) {
        File[] files = null;
        List<T> result = new ArrayList<T>();
        File directory = new File(directoryPath);
        if (directory.exists() && directory.isDirectory()) {
            files = directory.listFiles();
            ;
        } else {
            LOG.error("По указанному пути не удалось найти директорию");
        }
        if (files != null) {
            for (File file : files) {
                result.add(Marshaller.unMarshalling(file));
            }
        } else {
            LOG.error("Нет файлов для обработки.");
        }
        return result;
    }

    /**
     * Convert object in XML file in defined directory
     * @param obj
     * @param directoryPath
     * @param <T>
     */
    public static <T> void convertEntityToXML(T obj, String directoryPath) {
        count++;
        String filePath = obj.getClass().getSimpleName();
        File file = new File(directoryPath, filePath
                + count + "_file.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Marshaller.marshalling(obj, file);
    }
}
