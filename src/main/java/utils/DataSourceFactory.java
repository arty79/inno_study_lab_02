package utils;

import org.postgresql.ds.PGPoolingDataSource;

import javax.sql.DataSource;

/**
 * Produce Connection Pool for connect to DB
 *
 * @author Artem Panasyuk
 */
public class DataSourceFactory {

    /**
     * Get an object DataSource for connection to DB
     * @return dataSource
     */
    public static DataSource getMyPGDataSource() {
        PGPoolingDataSource source = new PGPoolingDataSource();
        source.setDataSourceName("CrmDB");
        source.setServerName("localhost");
        source.setDatabaseName("crm_inno");
        source.setUser("crm");
        source.setPassword("crm");
        source.setMaxConnections(10);
        return source;
    }
}
