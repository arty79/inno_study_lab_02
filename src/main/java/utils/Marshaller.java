package utils;

import com.innopolis.examples.xjc.Operation;
import com.innopolis.examples.xjc.Resource;
import com.innopolis.examples.xjc.Role;
import com.innopolis.examples.xjc.User;
import org.apache.log4j.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**This parametrized utils class marshall and unmarshall objects by JAXB
 *
 * @author  Artem Panasyuk
 */
public class Marshaller {
    private static final Logger LOG = Logger.getLogger(Marshaller.class);

    /**
     * Marshall object to XML file
     * @param obj
     * @param file
     */
    public static void marshalling(Object obj, File file) {
        FileWriter writer = null;
        try {
            writer = new FileWriter(file);
            //создание объекта Marshaller, который выполняет сериализацию
            JAXBContext context = JAXBContext.newInstance(User.class);
            javax.xml.bind.Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(obj, writer);
        } catch (IOException e) {
            LOG.error("Exception:" + e.getMessage());
        } catch (PropertyException e) {
            LOG.error("Exception:" + e.getMessage());
        } catch (JAXBException e) {
            LOG.error("Exception:" + e.getMessage());
        }
    }

    /**
     * UnMarshall object from file to XML
     * @param file
     * @param <T>
     * @return object
     */
    public static <T> T unMarshalling(File file) {
        T result = null;
        try (FileReader reader = new FileReader(file)) {
            JAXBContext context = JAXBContext.newInstance(User.class, Role.class,
                    Resource.class, Operation.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            result = (T) unmarshaller.unmarshal(reader);
        } catch (IOException e) {
            LOG.error("Exception:" + e.getMessage());
        } catch (JAXBException e) {
            LOG.error("Exception:" + e.getMessage());
        }
        return result;
    }
}
