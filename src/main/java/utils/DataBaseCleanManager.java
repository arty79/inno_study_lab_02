package utils;


import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.sql.*;

/**
 * Clear tables in DB
 *
 * @author Artem Panasyuk
 */
public class DataBaseCleanManager {

    private static final Logger LOG = Logger.getLogger(DataBaseCleanManager.class);
    private DataSource dataSource;

    public DataBaseCleanManager(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Clear data in tables in DB
     */
    public void truncate() {
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            statement.execute("DELETE from user_role;" +
                    "DELETE from operation_resource;" +
                    "DELETE from role_resource;" +
                    "DELETE from crm_user;" +
                    "DELETE from role;" +
                    "DELETE from resource;" +
                    "DELETE from operation;");
            LOG.info("Database have done cleared");
        } catch (SQLException e) {
            LOG.error("SQLException:" + e.getMessage());
        }
    }
}
