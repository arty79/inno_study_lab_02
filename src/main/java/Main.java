
import com.innopolis.examples.xjc.Role;
import com.innopolis.examples.xjc.User;
import service.dao.RoleDao;
import service.dao.UserDao;
import service.impl.RoleDaoImpl;
import service.impl.UserDaoImpl;
import utils.DataBaseCleanManager;
import utils.DataSourceFactory;
import utils.EntityXMLConverter;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {
    private static Map<Integer, Role> roleMap = new HashMap<>();
    private static UserDao userDao;
    private static RoleDao roleDao;

    public static void main(String[] args) {

        DataSource dataSource = DataSourceFactory.getMyPGDataSource();
        DataBaseCleanManager manager = new DataBaseCleanManager(dataSource);
        roleDao = new RoleDaoImpl(dataSource);
        userDao = new UserDaoImpl(dataSource);

/*        for (Role role : roleDao.findAll()) {
            roleMap.put(role.getId(), role);
            EntityXMLConverter.convertEntityToXML(role, "c:\\temp\\jaxb\\Role");
        }

        for (User user : userDao.findAll()) {
            EntityXMLConverter.convertEntityToXML(user, "c:\\temp\\jaxb\\User");
            if (user.getRole() != null && !user.getRole().isEmpty()) {
                for (Role role : user.getRole()) {
                    if (roleMap.containsKey(role.getId())) {
                        continue;
                    } else {
                        EntityXMLConverter.convertEntityToXML(role, "c:\\temp\\jaxb\\Role");
                    }
                }
            }
        }*/
        manager.truncate();
        List<User> users = EntityXMLConverter.convertXMLToEntity("c:\\temp\\jaxb\\User");
        List<Role> roles = EntityXMLConverter.convertXMLToEntity("c:\\temp\\jaxb\\Role");

        for (User user : users) {
            userDao.insert(user);
        }
        for (Role role : roles) {
            roleDao.insert(role);
        }
    }
}
