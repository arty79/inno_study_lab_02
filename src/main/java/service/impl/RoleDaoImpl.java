package service.impl;

import com.innopolis.examples.xjc.Role;
import org.apache.log4j.Logger;
import service.dao.RoleDao;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service implements DAO interface to insert Role objects into DB or get Role objects from DB
 *
 * @author Artem Panasyuk
 */
public class RoleDaoImpl implements RoleDao {

    private static final Logger LOG = Logger.getLogger(Role.class);
    private DataSource dataSource;

    public RoleDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Creates a <code>Role</code> object from DB by id
     *
     * @param id - identificator of object in DB table
     * @return a new default <code>Role</code> object
     */
    @Override
    public Role findRoleById(Integer id) {
        Role role = null;
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs =
                    statement.executeQuery("select * from role r where r.id = " + id);
            role = new Role();
            role.setId(id);
            while (rs.next()) {
                role.setName(rs.getString("name"));
                role.setCode(rs.getString("code"));
                role.setIsDeleted(rs.getBoolean("is_deleted"));
            }
        } catch (SQLException e) {
            LOG.error("SQLException:" + e.getMessage());
        }
        return role;
    }

    /**
     * Creates a list of <code>Role</code> objects from DB
     *
     * @return a new default <code>list Role</code> objects
     */
    @Override
    public List<Role> findAll() {
        Map<Integer, Role> map = new HashMap<>();
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs =
                    statement.executeQuery("select * from role");
            Role role = null;
            while (rs.next()) {
                Integer id = rs.getInt("id");
                role = map.get(id);
                if (role == null) {
                    role = new Role();
                    role.setId(id);
                    role.setName(rs.getString("name"));
                    role.setCode(rs.getString("code"));
                    role.setIsDeleted(rs.getBoolean("is_deleted"));
                    map.put(id, role);
                }
            }
        } catch (SQLException e) {
            LOG.error("SQLException:" + e.getMessage());
        }
        return new ArrayList<>(map.values());
    }

    /**
     * Insert object <code>Role</code> to DB
     * with defined id
     */
    @Override
    public void insert(Role role) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement =
                    connection.prepareStatement("INSERT INTO role(" +
                            "id, name, code, is_deleted)" +
                            " VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, role.getId());
            preparedStatement.setString(2, role.getName());
            preparedStatement.setString(3, role.getCode());
            preparedStatement.setBoolean(4, role.isIsDeleted());
            preparedStatement.execute();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                role.setId(generatedKeys.getInt(1));
                LOG.info("New Role inserted with id: " + role.getId());
            } else {
                LOG.info("Role with id: " + role.getId() + "is existed");
            }
        } catch (SQLException e) {
            LOG.error("SQLException:" + e.getMessage());
        }
    }
}
