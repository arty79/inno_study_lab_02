package service.impl;

import java.sql.*;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;

import com.innopolis.examples.xjc.Role;
import com.innopolis.examples.xjc.User;
import org.apache.log4j.Logger;
import service.dao.UserDao;

import javax.sql.DataSource;

/**
 * Service implements DAO interface to insert User objects into DB
 * or get User objects from DB
 *
 * @author Artem Panasyuk
 */
public class UserDaoImpl implements UserDao {

    private static final Logger LOG = Logger.getLogger(User.class);
    private DataSource dataSource;

    public UserDaoImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Creates a <code>User</code> object from DB by id
     *
     * @param id - identificator of object in DB table
     * @return a new default <code>User</code> object
     */
    @Override
    public User findUserById(Integer id) {
        User user = new User();
        user.setId(id);
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs =
                    statement.executeQuery("select cu.id, cu.login, cu.first_name, cu.user_password, cu.email," +
                            "  cu.last_name, cu.is_active, cu.is_deleted,r.id as role_id, r.name,r.code, r.is_deleted as " +
                            "role_is_deleted from crm_user cu" +
                            "  left join user_role ur" +
                            "    on cu.id = ur.crm_user_id" +
                            "  left join role r on r.id = ur.role_id where cr.id = " + id);
            while (rs.next()) {
                if (user == null) {
                    user = new User();
                    user.setId(id);
                    user.setFirstName(rs.getString("first_name"));
                    user.setLastName(rs.getString("last_name"));
                    user.setEmail(rs.getString("email"));
                    user.setUserPassword(rs.getString("user_password"));
                    user.setLogin(rs.getString("login"));
                    user.setIsActive(rs.getBoolean("is_active"));
                    user.setIsDeleted(rs.getBoolean("is_deleted"));
                    user.setRole(new ArrayList<>());
                }

                Integer roleId = rs.getInt("role_id");

                if (roleId > 0) {
                    Role role = new Role();
                    role.setId(roleId);
                    role.setName(rs.getString("name"));
                    role.setCode(rs.getString("code"));
                    role.setIsDeleted(rs.getBoolean("is_deleted"));
                    user.getRole().add(role);
                }
            }
        } catch (SQLException e) {
            LOG.error("SQLException:" + e.getMessage());
        }
        return user;
    }

    /**
     * Creates a list of <code>User</code> objects from DB
     *
     * @return a new default <code>list User</code> objects
     */
    @Override
    public List<User> findAll() {
        Map<Integer, User> map = new HashMap<>();
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs =
                    statement.executeQuery("select cu.id, cu.login, cu.first_name, cu.user_password, cu.email," +
                            "  cu.last_name, cu.is_active, cu.is_deleted,r.id as role_id, r.name,r.code, r.is_deleted as " +
                            "role_is_deleted from crm_user cu" +
                            "  left join user_role ur" +
                            "    on cu.id = ur.crm_user_id" +
                            "  left join role r on r.id = ur.role_id");
            User user = null;
            while (rs.next()) {
                Integer id = rs.getInt("id");
                user = map.get(id);
                if (user == null) {
                    user = new User();
                    user.setId(id);
                    user.setFirstName(rs.getString("first_name"));
                    user.setLastName(rs.getString("last_name"));
                    user.setEmail(rs.getString("email"));
                    user.setUserPassword(rs.getString("user_password"));
                    user.setLogin(rs.getString("login"));
                    user.setIsActive(rs.getBoolean("is_active"));
                    user.setIsDeleted(rs.getBoolean("is_deleted"));
                    user.setRole(new ArrayList<>());
                    map.put(id, user);
                }
                Integer roleId = rs.getInt("role_id");
                if (roleId > 0) {
                    Role role = new Role();
                    role.setId(roleId);
                    role.setName(rs.getString("name"));
                    role.setCode(rs.getString("code"));
                    role.setIsDeleted(rs.getBoolean("is_deleted"));
                    user.getRole().add(role);
                }
            }
        } catch (SQLException e) {
            LOG.error("SQLException:" + e.getMessage());
        }
        return new ArrayList<>(map.values());
    }

    /**
     * Insert object <code>User</code> to DB
     */
    @Override
    public void insert(User user) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement =
                    connection.prepareStatement("INSERT INTO crm_user(" +
                            "id, login, user_password, first_name, last_name, email, is_active, is_deleted)" +
                            " VALUES (?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, user.getId());
            preparedStatement.setString(2, user.getLogin());
            preparedStatement.setString(3, user.getUserPassword());
            preparedStatement.setString(4, user.getFirstName());
            preparedStatement.setString(5, user.getLastName());
            preparedStatement.setString(6, user.getEmail());
            preparedStatement.setBoolean(7, user.isIsActive());
            preparedStatement.setBoolean(8, user.isIsDeleted());
            preparedStatement.execute();
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if (generatedKeys.next()) {
                user.setId(generatedKeys.getInt(1));
                LOG.info("New User inserted with id: " + user.getId());
            }
            if (user.getRole() != null && !user.getRole().isEmpty()) {
                for (Role role : user.getRole()) {
                    preparedStatement =
                            connection.prepareStatement("INSERT INTO role(" +
                                    "id, name, code, is_deleted)" +
                                    " VALUES (?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
                    preparedStatement.setInt(1, role.getId());
                    preparedStatement.setString(2, role.getName());
                    preparedStatement.setString(3, role.getCode());
                    preparedStatement.setBoolean(4, role.isIsDeleted());
                    preparedStatement.executeUpdate();
                    generatedKeys = preparedStatement.getGeneratedKeys();
                    if (generatedKeys.next()) {
                        role.setId(generatedKeys.getInt(1));
                        LOG.info("New Role inserted with id: " + role.getId());
                    }
                    preparedStatement =
                            connection.prepareStatement("INSERT INTO user_role(" +
                                    "crm_user_id, role_id)" +
                                    " VALUES (?, ?)");
                    preparedStatement.setInt(1, user.getId());
                    preparedStatement.setInt(2, role.getId());
                    preparedStatement.executeUpdate();
                }
            }
        } catch (SQLException e) {
            LOG.error("SQLException:" + e.getMessage());
        }
    }
}
