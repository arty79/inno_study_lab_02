package service.dao;

import com.innopolis.examples.xjc.User;

import java.util.List;

/**DAO interface to insert User objects into DB or get User objects from DB
 *
 * @author  Artem Panasyuk
 */
public interface UserDao {
    //List<User> findAll();
    //List<User> findByFirstName(String firstName);
    User findUserById(Integer id);
    List<User> findAll();
    void insert(User user);
    //void update(User user);
}
