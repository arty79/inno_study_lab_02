package service.dao;

import com.innopolis.examples.xjc.Role;

import java.util.List;

/**Service implements DAO interface to insert Role objects into DB or get Role objects from DB
 *
 * @author  Artem Panasyuk
 */
public interface RoleDao {
    List<Role> findAll();
    Role findRoleById(Integer id);
    //String findRoleByCode(String code);
    void insert(Role role);
    //void update(Role role);
}
